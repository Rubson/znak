<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\BookType;
use App\Entity\Book;
use Symfony\Component\HttpFoundation\Request;

class BookController extends AbstractController
{

    public function add(Request $request)
    {
        $task = new Book();
        $form = $this->createForm(BookType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->save($task);

            return $this->redirectToRoute('book_list');
        }

        return $this->render('book/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function list()
    {
        $product = $this->getDoctrine()
            ->getRepository(Book::class)
            ->findAll();

        if (!$product) {
            throw $this->createNotFoundException(
                'List is empty'
            );
        }

        return $this->render('book/list.html.twig', [
            'bookList' => $product,
        ]);
    }

    public function edit(Request $request, int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $book = $entityManager->getRepository(Book::class)->find($id);

        if (!$book) {
            throw $this->createNotFoundException(
                'Nie znaleziono książki o id: '.$id
            );
        }

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->save($task);

            return $this->redirectToRoute('book_list');
        }

        return $this->render('book/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Należałoby zrobić odpowiednie repozytorium
     * @param $data
     */
    private function save($data)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($data);
        $entityManager->flush();
    }
}
